<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Etp extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'etp',
        /* 'description',
        'adresse',
        'ville',
        'codepostal',
        'telephone',
        'email',
        'site',
        'slug',
        'user_id', */
    ];

    /**
     * relationship
     */
    public function formations() {
        return $this->hasMany('App\Formation');
    }
}
