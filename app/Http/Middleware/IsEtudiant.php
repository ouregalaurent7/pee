<?php

namespace App\Http\Middleware;

use Closure;

class IsEtudiant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->user()->role_id != 4) {

            $message = "Vous ne disposez pas des droits nécessaires pour accéder à cette page.";

            return redirect(route('forbiden'))->with('message', $message);

        }
        return $next($request);
    }
}
