<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // admin = 1, editeur = 2, enseignant = 3, etudiant = 4
        if( $request->user()->role_id != 1 && $request->user()->role_id != 2) {

            $message = "Vous ne disposez pas des droits nécessaires pour accéder à cette page.";

            return redirect(route('forbiden'))->with('message', $message);

        }
        return $next($request);
    }

}
