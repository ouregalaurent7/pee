<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use App\Album;
use App\Photo;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MediasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function photo($id){
        $photos = Photo::where('album_id',$id)->orderBy('id','desc')->get();
        $idalbum=$id;
        return view('medias.photos',compact('photos','idalbum'));
    }

    public function updatePhoto($id , $type){
        //dd($id,$type);

        $photo = Photo::find($id);
        if($type=='enabled'){
            $photo->status = '1';
            $message = redirect()->back()->with('success','Votre photo a été activé');
        }else{
            $photo->status = '0';
            $message = redirect()->back()->with('success','Votre photo a été désactivé');
        }
        $photo->save();

        return $message ;
    }

    public function deletPhoto($id){
        //dd($id);
        $folderName ='assets/mediatheques/photos/';
        $photo = Photo::find($id);
        if (!empty($photo->img)) {
            unlink($folderName.$photo->img);
        }
        $photo->delete();
        return redirect()->back()->with('success','Votre photo a été supprimé');

    }

    public function allDeletPhoto(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        $folderName ='assets/mediatheques/photos/';

        foreach ($ids as $id){
            $photo = Photo::find($id);
            if (!empty($photo->img)) {
                unlink($folderName.$photo->img);
            }
            $photo->delete();
        }
        echo $data = '1';
    }

    public function allActPhoto(Request $request){
        //dd($id,$type);
        $ids = explode(',',$request->value);

        foreach ($ids as $id){
            $photo = Photo::find($id);
            $photo->status = '1';
            $photo->save();
        }
        echo $data = '1';
    }

    public function allInactPhoto(Request $request){
        //dd($id,$type);
        $ids = explode(',',$request->value);

        foreach ($ids as $id){
            $photo = Photo::find($id);
            $photo->status = '0';
            $photo->save();
        }
        echo $data = '1';
    }

    public function storePhoto(Request $request){
        //dd($request->file('fileUser'));
        $files=$request->file('fileUser');

        //dd($fileSize);
        try{
            foreach ($files as $file):
            $fileSize=$file->getSize();
                if($fileSize <= 3000263):
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName ='assets/mediatheques/photos/';
                    $picture = Str::random(8).'.'. $extension;

                    $photo= new Photo();
                    $photo->img = $picture;
                    $photo->album_id = $request->album;
                    $photo->save();

                    $file->move($folderName,$picture);

                endif;
            endforeach;

            return redirect()->back()->with('success','Félicitation ! votre photo a été ajouté');

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }


    //VIDEO

    public function videos(){
        $videos = Video::orderBy('id','desc')->get();
        return view('medias.videos',compact('videos'));
    }

    public function storeVideo(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'lien' =>'max:255|url',
        ],[
            'url' => 'Le lien fourni n\'est pas correct.',
            'max' => 'Le champ :attribute dépasse :min caractères.'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $videoId = Youtube::parseVidFromURL($request->lien);
            $infos = Youtube::getVideoInfo($videoId);
            //dd($infos);
            $video = new Video();
            $video->lien = $request->lien ;
            $video->title = $infos->snippet->title;
            $video->img = $infos->snippet->thumbnails->high->url;
            $video->save();

            return redirect()->route('videos')->with('success','✔ Félicitation ! vous venez d\' ajoute une vidéo');
        }
    }

    public function deletVideo($id){
        $video = Video::find($id);
        $video->delete();
        return redirect()->back()->with('success','Votre vidéo a été supprimé');
    }

    public function allDeletVideo(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        foreach ($ids as $id){
            $video = Video::find($id);
            $video->delete();
        }
        echo $data = '1';
    }

    //ALBUM

    public function album(){
        $albums = Album::orderBy('id','desc')->with('photos')->get();
        return view('medias.albums',compact('albums'));
    }

    public function deletAlbum($id){
        //dd($id);
        $folderName ='assets/mediatheques/photos/';
        $album = Album::find($id);
        $photos = Photo::where('album_id',$id)->get();

        //SUPPRIMER LES PHOTOS DES ALBUMS
        foreach ($photos as $photo){
            $pho = Photo::find($photo->id);
            if (!empty($pho->img)) {
                unlink($folderName.$pho->img);
            }
            $pho->delete();
        }

        if (!empty($album->cover)) {
            unlink($folderName.$album->cover);
        }
        $album->delete();
        return redirect()->back()->with('success','Votre album a été supprimé');

    }

    public function allDeletAlbum(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        $folderName ='assets/mediatheques/photos/';

        foreach ($ids as $id){
            $album = Album::find($id);
            $photos = Photo::where('album_id',$id)->get();

            //SUPPRIMER LES PHOTOS DES ALBUMS
            foreach ($photos as $photo){
                $pho = Photo::find($photo->id);
                if (!empty($pho->img)) {
                    unlink($folderName.$pho->img);
                }
                $pho->delete();
            }

            if (!empty($album->cover)) {
                unlink($folderName.$album->cover);
            }
            $album->delete();
        }
        echo $data = '1';
    }

    public function editAlbum($id){
        //dd($id);
        $album = Album::find($id);
        return view('medias.editalbum',compact('album'));
    }

    public function storeAlbum(Request $request){
        //dd($request->all());
        $file=$request->file('fileUser');

        //dd($fileSize);
        try{
            $fileSize=$file->getSize();
            if($fileSize <= 3000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName ='assets/mediatheques/photos/';
                $picture = Str::random(8).'.'. $extension;

                $album= new Album();
                $album->title = $request->title;
                $album->slug = Str::slug($request->title);
                $album->cover = $picture;
                $album->save();

                $file->move($folderName,$picture);
                return redirect()->back()->with('success','Félicitation ! votre album a été ajouté');
            endif;

            return redirect()->back()->with('error','La photo de couverture de votre album est trop elevé');

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    public function updateAlbum(Request $request){
        //dd($request->all());

        if($request->idalbum){
            try{

                $album = Album::find($request->idalbum);
                $album->title = $request->title;
                $album->slug = Str::slug($request->title);

                if($request->hasFile('fileUser')){
                    $file=$request->file('fileUser');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName ='assets/mediatheques/photos/';
                    $picture = Str::random(8).'.'. $extension;

                    if (!empty($album->cover)) {
                        unlink($folderName.$album->cover);
                    }

                    $file->move($folderName,$picture);
                    $album->cover = $picture;
                }

                $album->save();
                return redirect()->route('albums')->with('success','Votre album a été modifié');

            }catch (\Exception $e){
                dd($e->getMessage());
            }
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez recommencez');

    }
}
