<?php

namespace App\Http\Controllers;

use App\Actu;
use App\Categorie;
use App\Mail\NotifMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{
    public function index()
    {
        // $new6 = Actu::where('status',1)->groupBy('id')->take(6)->get();
        $new6 = DB::table('actualites_categories')
            ->join('actualites', 'actualites.id', '=', 'actualites_categories.actu_id')
            ->join('categories', 'categories.id', '=', 'actualites_categories.cat_id')
            ->select('actualites.*','actualites.slug as slug_actu','actualites.created_at as created_at_actu', 'categories.*')
            //->groupBy('categories.id')
            ->take(6)
            ->get();

        $cat6 = Categorie::take(6)->get();
        //dd($new6);
        return view('web/index',compact('cat6'));
    }

    public function getActu($id){
        $cat = Actu::whereIn('cat_id',$id)->get();
        dd($cat);
        return $cat;
    }

    public function about()
    {
        return view('web.about');
    }

    public function formation()
    {
        return view('web.formation');
    }

    public function offre()
    {
        return view('web.offre');
    }

    public function offreItem()
    {
        return view('web.offre_item');
    }

    public function promo()
    {
        return view('web.promo');
    }

    public function promoItem()
    {
        return view('web.promo_item');
    }

    public function news()
    {
        return view('web.news');
    }

    public function newsItem()
    {
        return view('web.news_item');
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function terme()
    {
        return view('web.termes');
    }

    public function politique()
    {
        return view('web.politique');
    }


    public function store_user_etud(Request $request) {
        try {
            if( $request->nm1 != '' && $request->em2 != '' && $request->pass3 != '' ) {
                $cheking = User::where('email',$request->em2)->first();
                if($cheking!=null) {
                    $message = "Cette adresse email appatient déjà à un autre étudiant !" ;
                    return back()->with('erro', $message);
                }
                $usr = new User();
                $usr->role_id = 4;//étudiant
                $usr->name = ucwords($request->nm1);
                $usr->email = $request->em2;
                $usr->password = Hash::make($request->pass3);
                $usr->save();
                // send email to user
                $data = array(
                    'name' => $usr->name,
                    'email' => env('MAIL_FROM_ADDRESS'),
                    'subject' => 'Inscription réussie | '.env('APP_NAME'),
                    'message' => 'Votre compte a été créé avec succès. Veuillez-vous connecter pour continuer !',
                    'view' => 'emails.newetudiant',
                    'user'  => $usr,
                    'link' => route('login'),
                    'attachment' => ''
                );
                Mail::to($usr->email)->send(new NotifMail($data));
                // send email to admin
                $to = User::whereIn('role_id',[1,2])->where('status',1)->pluck('email');
                $mss = 'Un nouveau étudiant vient de s\'inscrire sur le site. Veuillez vérifier ses informations.';
                $data = [
                    'name' => env('APP_NAME'),
                    'email' => env('MAIL_FROM_ADDRESS'),
                    'subject' => 'Nouveau étudiant | '.env('APP_NAME'),
                    'message' => $mss,
                    'view'  => 'emails.newetudiant',
                    'user'  => $usr,
                    'link' => route('login'),
                    'attachment' => ''
                ];
                foreach($to as $r) {
                    Mail::to($r)->send(new NotifMail($data));
                }

                $message = "<h3>Bienvenue ".$usr->name." ! </h3> Votre compte a été créé avec succès. <br> Veuillez-vous <a href='".route('login')."'>connecter</a> pour continuer ! <br> <a href='".route('login')."' class='btn btn-info'>Connexion</a> " ;
                return back()->with('success', $message);
            }
            else {

            }
        }
        catch(\Exception $e) {
            $message = $e->getMessage();
            return back()->with('error', $message);
        }

    }

}
