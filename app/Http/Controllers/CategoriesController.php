<?php

namespace App\Http\Controllers;

use App\Actu;
use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $cats = Categorie::all();
        return view('categories.index',compact('cats'));
    }

    public function store(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Categorie();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->save();

            return redirect()->route('categories')->with('success','✔ Félicitation ! vous venez d\' ajoute une catégorie');
        }
    }

    public function edit($id){
        $cat = Categorie::find($id);
        return view('categories.edit',compact('cat'));
    }

    public function update(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required|min:6',
            'idActu'=>'required'
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Categorie::where('slug',$request->idActu)->first();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->save();

            return redirect()->route('categories')->with('success','Vous venez de modifier une catégorie');
        }
    }

    public function delet($id){
        //dd($id);
        if($id){
            $cat = Categorie::findOrFail($id);
            $cat->delete();
            return redirect()->route('categories')->with('success','Vous venez de supprimer une catégorie');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $cat = Categorie::findOrFail($id);
            $cat->delete();
        }
        echo $data = '1';
    }

//    public function getNbArticle($id){
//        dd($id);
//        $actu = Actu::where('cat_id','like');
//    }
}
