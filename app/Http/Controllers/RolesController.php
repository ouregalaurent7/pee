<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $roles = Role::with('users')->get();
        return view('roles.index',compact('roles'));
    }

    public function store(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Role();
            $cat->libelle = $request->nom ;
            $cat->save();

            return redirect()->route('roles')->with('success','✔ Félicitation ! vous venez d\' ajoute un rôle');
        }
    }

    public function edit($id){
        $role = Role::find($id);
        return view('roles.edit',compact('role'));
    }

    public function update(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'idrole'=>'required'
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Role::where('id',$request->idrole)->first();
            $cat->libelle = $request->nom ;
            $cat->save();

            return redirect()->route('roles')->with('success','Vous venez de modifier un rôle');
        }
    }

    public function delet($id){
        //dd($id);
        if($id){
            $role = Role::findOrFail($id);
            $role->delete();
            return redirect()->route('roles')->with('success','Vous venez de supprimer un rôle');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);

        foreach ($ids as $id){
            $role = Role::findOrFail($id);
            $role->delete();
        }
        //return redirect()->route('roles')->with('success','Suppression effectuer');
        echo $data = '1';
    }
}
