<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index(){
        $users = User::whereIn('role_id',[1,2])->with('roles')->with('articles')->orderBy('id','desc')->get();
        return view('users.index',compact('users'));
    }

    public function create(){
        $roles = Role::whereIn('role_id',[1,2])->get();
        return view('users.create',compact('roles'));
    }

    public function store(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string|max:255',
            'role_id' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()->route('users.create')->withErrors($validator->errors());
        }else{
            $user = new User();
            $user->name=$request->nom;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            $user->role_id=$request->role_id;
            $user->save();

            return redirect()->route('users')->with('success','✔ Félicitation ! vous venez d\' ajoute un utilisateur');
        }

    }

    public function edit($id){
        $user = User::find($id);
        $roles = Role::whereIn('role_id',[1,2])->get();
        return view('users.edit',compact('user','roles'));
    }

    public function update(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string|max:255',
            'role_id' => 'required|string',
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }else{
            $user = User::find($request->iduser);
            $user->name=$request->nom;
            $user->email=$request->email;

            if($request->password != null){
                $user->password=Hash::make($request->password);
            }

            $user->role_id=$request->role_id;
            $user->save();

            return redirect()->route('users')->with('success','Vous venez de modifier un utilisateur');
        }
    }

    public function delet($id){
        if($id){
            $user = User::findOrFail($id);
            $user->delete();
            return redirect()->route('users')->with('success','Vous venez de supprimer un utilisateur');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $user = User::findOrFail($id);
            $user->delete();
        }
        echo $data = '1';
    }

}
