<?php

namespace App\Http\Controllers;

use App\Formation;
use Illuminate\Http\Request;

class FormationsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index(){
        //get formations grouped by statut
        $statuts = [
            1 => 'Brouillon',
            2 => 'En ligne',
        ];
        $data = [];
        foreach ($statuts as $key=>$statut) {
            $data[$statut] = Formation::where('statut',$key)->where('debut','>=',date('Y-m-d'))->get();
        }
        $data['Terminer'] = Formation::where('debut','<',date('Y-m-d'))->get();
        return view('formations.index',compact('data'));
    }

    public function create(){
        return view('formations.create');
    }
}
