<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actu extends Model
{
    use SoftDeletes;

    protected $table = 'actualites';
    protected $dates = ['deleted_at'];

    public function author(){
        return $this->belongsTo('App\User','auteur');
    }
}
