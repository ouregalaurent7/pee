<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formation_demande extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'etudiant_id',
        'formation_id',
        'statut',//0=non traité, 1=accepté, 2=refusé
    ];
    /**
     * relationship
     */
    public function etudiant() {
        return $this->belongsTo('App\User', 'etudiant_id');
    }
    public function formation() {
        return $this->belongsTo('App\Formation');
    }
}
