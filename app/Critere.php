<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Critere extends Model
{
    protected $fillable = [
        'libelle',
        'valeur',
    ];
    /**
     * relationship
     */
    public function formations() {
        return $this->belongsToMany('App\Formation');
    }
}
