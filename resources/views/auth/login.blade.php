<!DOCTYPE html>
<html lang="fr">
	<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>PEE | Connexion</title>

		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/fronts/img/favicon.png') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.carousel.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fronts/plugins/feather/feather.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/style.css') }}">

	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper log-wrap">

			<div class="row">

				<!-- Login Banner -->
				<div class="col-md-6 login-bg">
					<div class="owl-carousel login-slide owl-theme">
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img-2.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img-3.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>

					</div>
				</div>
				<!-- /Login Banner -->

				<div class="col-md-6 login-wrap-bg">

					<!-- Login -->
					<div class="login-wrapper">
						<div class="loginbox">
							<div class="w-100">
								<div class="img-logo">
									<img src="{{asset('assets/fronts/img/logo.png')}}" width="300" class="img-fluid" alt="Logo">
									<div class="back-home">
										<a href="{{route('web')}}">Retour au site</a>
									</div>
								</div>
								<h1>Connexion</h1>
								<form method="POST" action="{{ route('login') }}">
                                    @csrf
									<div class="form-group">
										<label class="form-control-label">Email</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

									</div>
									<div class="form-group">
										<label class="form-control-label">Mot de passe</label>
										<div class="pass-group">
                                            <input id="password" type="password" class="form-control pass-input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                            <span class="feather-eye toggle-password"></span>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
									</div>

                                    @if (Route::has('password.request'))
									<div class="forgot">
										<span><a class="forgot-link" href="{{ route('password.request') }}">Mot de passe oublier ?</a></span>
									</div>
                                    @endif

									<div class="remember-me">
										<label class="custom_check mr-2 mb-0 d-inline-flex remember-me"> Se souvenir de moi
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
											<span class="checkmark"></span>
										</label>
									</div>
									<div class="d-grid">
										<button class="btn btn-primary btn-start" type="submit">Connexion</button>
									</div>
								</form>
							</div>
						</div>
						<div class="google-bg text-center">
							<span><a href="#">Ou connectez-vous avec</a></span>
							<div class="sign-google">
								<ul>
									<li><a href="#"><img src="{{asset('assets/fronts/img/net-icon-01.png')}}" class="img-fluid" alt="Logo"> Sign In using Google</a></li>
									<li><a href="#"><img src="{{asset('assets/fronts/img/net-icon-02.png')}}" class="img-fluid" alt="Logo">Sign In using Facebook</a></li>
								</ul>
							</div>
							<p class="mb-0">Nouvel utilisateur ? <a href="{{route('register')}}">Créer un compte</a></p>
						</div>
					</div>
					<!-- /Login -->

				</div>

			</div>

	   </div>
	   <!-- /Main Wrapper -->

       <script src="{{asset('assets/fronts/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/script.js')}}"></script>

	</body>
</html>
