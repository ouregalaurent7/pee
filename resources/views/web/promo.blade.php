@extends('web.main')
@section('title', "PEE | Promotions")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Promotions</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    @for ($i = 1; $i <= 5; $i++)
                        <div class="col-md-4 col-sm-12">
                            <!-- Blog Post -->
                            <div class="blog grid-modern">
                                <div class="blog-image">
                                    <a href="blog-details.html"><img class="img-fluid" src="{{asset('assets/fronts/img/blog/blog-14.jpg')}}" alt=""></a>
                                </div>
                                <div class="blog-modern-box">
                                    <h3 class="blog-title"><a href="blog-details.html">Learn Webs Applications Development from Experts</a></h3>
                                    <div class="blog-info clearfix mb-0">
                                        <div class="post-left">
                                            <ul>
                                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="">Jan 20, 2022</li>
                                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-24.svg')}}" alt="">Programming, Angular</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>

                <!-- Blog pagination -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="pagination lms-page">
                            <li class="page-item prev">
                                <a class="page-link" href="javascript:void(0);" tabindex="-1"><i class="fas fa-angle-left"></i></a>
                            </li>
                            <li class="page-item first-page active">
                                <a class="page-link" href="javascript:void(0);">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">4</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">5</a>
                            </li>
                            <li class="page-item next">
                                <a class="page-link" href="javascript:void(0);"><i class="fas fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Blog pagination -->

            </div>
        </div>
    </div>
</section>

@endsection
