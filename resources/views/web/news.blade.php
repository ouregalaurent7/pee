@extends('web.main')
@section('title', "PEE | Actualités")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Actualités</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">

                @for ($i = 1; $i <= 5; $i++)
                <div class="blog">
                    <div class="blog-image">
                        <a href="{{route('w.newsItem')}}"><img class="img-fluid" src="{{asset('assets/fronts/img/blog/blog-05.jpg')}}" alt="Post Image"></a>
                    </div>
                    <div class="blog-info clearfix">
                        <div class="post-left">
                            <ul>
                                <li>
                                    <div class="post-author">
                                        <a href="instructor-profile.html"><img src="{{asset('assets/fronts/img/user/user.jpg')}}" alt="Post Author"> <span>Ruby Perrin</span></a>
                                    </div>
                                </li>
                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="">April 20, 2022</li>
                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-23.svg')}}" alt="">Programming, Web Design</li>
                            </ul>
                        </div>
                    </div>
                    <h3 class="blog-title"><a href="{{route('w.newsItem')}}">Learn Webs Applications Development from Experts</a></h3>
                    <div class="blog-content blog-read">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti. Sed egestas, ante et vulputate volutpat, eros pede […]</p>
                        <a href="{{route('w.newsItem')}}" class="read-more btn btn-primary">Lire Plus</a>
                    </div>
                </div>
                @endfor

                <!-- Blog pagination -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="pagination lms-page">
                            <li class="page-item prev">
                                <a class="page-link" href="javascript:void(0);" tabindex="-1"><i class="fas fa-angle-left"></i></a>
                            </li>
                            <li class="page-item first-page active">
                                <a class="page-link" href="javascript:void(0);">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">4</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0);">5</a>
                            </li>
                            <li class="page-item next">
                                <a class="page-link" href="javascript:void(0);"><i class="fas fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Blog pagination -->

            </div>

            <!-- Blog Sidebar -->
            <div class="col-lg-3 col-md-12 sidebar-right theiaStickySidebar">

                <!-- Search -->
                <div class="card search-widget blog-search blog-widget">
                    <div class="card-body">
                        <form class="search-form">
                            <div class="input-group">
                                <input type="text" placeholder="Recherche..." class="form-control">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /Search -->

                <!-- Latest Posts -->
                <div class="card post-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Actualités récentes</h4>
                    </div>
                    <div class="card-body">
                        <ul class="latest-posts">
                            <li>
                                <div class="post-thumb">
                                    <a href="blog-details.html">
                                        <img class="img-fluid" src="assets/img/blog/blog-01.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-info">
                                    <h4>
                                        <a href="blog-details.html">Learn Webs Applications Development from Experts</a>
                                    </h4>
                                    <p><img class="img-fluid" src="assets/img/icon/icon-22.svg" alt="">Juin 14, 2022</p>
                                </div>
                            </li>
                            <li>
                                <div class="post-thumb">
                                    <a href="blog-details.html">
                                        <img class="img-fluid" src="assets/img/blog/blog-02.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-info">
                                    <h4>
                                        <a href="blog-details.html">Expand Your Career Opportunities With Python</a>
                                    </h4>
                                    <p><img class="img-fluid" src="assets/img/icon/icon-22.svg" alt=""> 3 Dec 2019</p>
                                </div>
                            </li>
                            <li>
                                <div class="post-thumb">
                                    <a href="blog-details.html">
                                        <img class="img-fluid" src="assets/img/blog/blog-03.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-info">
                                    <h4>
                                        <a href="blog-details.html">Complete PHP Programming Career Guideline</a>
                                    </h4>
                                    <p><img class="img-fluid" src="assets/img/icon/icon-22.svg" alt=""> 3 Dec 2019</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Latest Posts -->


                <!-- Archives Categories -->
                <div class="card category-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Archives</h4>
                    </div>
                    <div class="card-body">
                        <ul class="categories">
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Janvier 2023 </a></li>
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Février 2023 </a></li>
                        </ul>
                    </div>
                </div>
                <!-- /Archives Categories -->

                <!-- Tags -->
                <div class="card tags-widget blog-widget tags-card">
                    <div class="card-header">
                        <h4 class="card-title">Mots clés</h4>
                    </div>
                    <div class="card-body">
                        <ul class="tags">
                            <li><a href="javascript:void(0);" class="tag">Entrepreneuriat</a></li>
                            <li><a href="javascript:void(0);" class="tag">Clientèle</a></li>
                            <li><a href="javascript:void(0);" class="tag">Commercialisation</a></li>
                            <li><a href="javascript:void(0);" class="tag">Marketing</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /Tags -->

            </div>
            <!-- /Blog Sidebar -->

        </div>
    </div>
</section>

@endsection
