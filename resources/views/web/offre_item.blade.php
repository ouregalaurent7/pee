@extends('web.main')
@section('title', "PEE | Offres d'emploi")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Offres</li>
                            <li class="breadcrumb-item active" aria-current="page">Offres d'emploi</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content share-knowledge-">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">

                <!-- Filter -->
                <div class="showing-list">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="show-filter add-course-info">
                                <form action="#">
                                    <div class="row gx-2 align-items-center">
                                        <div class="col-md-8 col-item">
                                            <div class=" search-group">
                                                <i class="feather-search"></i>
                                                <input type="text" class="form-control" placeholder="Recherche" >
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-item">
                                            <div class="form-group select-form mb-0">
                                                <select class="form-select select" id="sel1" name="sellist1">
                                                  <option>Publiées récemment </option>
                                                  <option>type 1</option>
                                                  <option>type 2</option>
                                                  <option>type 3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Filter -->

                <div class="all-course">
                    <div class="row">
                        @for ($i = 1; $i <= 5; $i++)
                        <div class="col-xl-12 col-lg-12 col-md-6 col-12">
                            <div class="course-box-three">
                                <div class="course-three-item">
                                    <div class="course-three-content ">
                                        <div class="course-three-text">
                                            <a href="javascript:void();">
                                                <p>Ressources Humaines</p>
                                                <h3 class="title instructor-text">Rem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat</h3>
                                            </a>
                                        </div>

                                        <div class="student-counts-info d-flex align-items-center">
                                            <div class="students-three-counts d-flex align-items-center">
                                                <p style="margin-left:0 ;"><span class="fa fa-calendar"> </span> &nbsp; Publiée le : <b>12/03/2023</b></p>
                                            </div>
                                        </div>

                                        <div class="price-three-group d-flex align-items-center justify-content-between justify-content-between">
                                            <div class="price-three-view d-flex align-items-center">
                                                <div class="course-price-three">
                                                    <a href="javascript:void();" class="btn btn-action">Voir les détails</a>
                                                </div>
                                            </div>
                                            <div class="price-three-time d-inline-flex align-items-center">
                                                <i class="fa-regular fa-clock me-2"></i>
                                                <span>Date limit : <b>29/05/2023</b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>

                <!-- /pagination -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="pagination lms-page">
                            <li class="page-item prev">
                                <a class="page-link" href="javascript:void(0)" tabindex="-1"><i class="fas fa-angle-left"></i></a>
                            </li>
                            <li class="page-item first-page active">
                                <a class="page-link" href="javascript:void(0)">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">4</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">5</a>
                            </li>
                            <li class="page-item next">
                                <a class="page-link" href="javascript:void(0)"><i class="fas fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /pagination -->

            </div>
            <div class="col-lg-3 theiaStickySidebar">
                <div class="filter-clear">
                    <div class="clear-filter d-flex align-items-center">
                        <h4><i class="feather-filter"></i>Filtrer</h4>
                        <div class="clear-text">
                            <p>EFFACER</p>
                        </div>
                    </div>

                    <!-- Search Filter -->
                    <div class="card search-filter ">
                        <div class="card-body">
                            <div class="filter-widget mb-0">
                                <div class="categories-head d-flex align-items-center">
                                    <h4>Ordonner par date</h4>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div>
                                    <label class="custom_check custom_one">
                                        <input type="radio" name="select_specialist" >
                                        <span class="checkmark"></span>  Date de publication
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check custom_one mb-0">
                                        <input type="radio" name="select_specialist" checked>
                                        <span class="checkmark"></span>  Date de fin
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Search Filter -->

                    <!-- Search Filter -->
                    <div class="card search-filter">
                        <div class="card-body">
                            <div class="filter-widget mb-0">
                                <div class="categories-head d-flex align-items-center">
                                    <h4>Trier par catégories</h4>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist" >
                                        <span class="checkmark"></span> Backend (3)

                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist" >
                                        <span class="checkmark"></span>  CSS (2)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist">
                                        <span class="checkmark"></span>  Frontend (2)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist" checked>
                                        <span class="checkmark"></span> General (2)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist" checked>
                                        <span class="checkmark"></span> IT & Software (2)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist">
                                        <span class="checkmark"></span> Photography (2)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check">
                                        <input type="checkbox" name="select_specialist">
                                        <span class="checkmark"></span>  Programming Language (3)
                                    </label>
                                </div>
                                <div>
                                    <label class="custom_check mb-0">
                                        <input type="checkbox" name="select_specialist">
                                        <span class="checkmark"></span>  Technology (2)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Search Filter -->



                </div>
            </div>
        </div>
    </div>
</section>

@endsection
