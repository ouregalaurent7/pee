@extends('web.main')
@section('title', 'Accueil')

@section('content')
<style>
	.category-hero{
		width: 100%;
		height: 500px;

		position: relative;
		margin-bottom: 300px;
		text-align: center;
	}
	.category-hero img{
		width: 100%;
		height: 100%;

		object-fit: cover;
		object-position: 0 0;
	}
	.category-hero .contenu{
		position: absolute;
		width: 40%;
		height: auto;

		left: 50%;
		bottom: -60%;
		margin-left: -20%;
		background-color: #fff;
		box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
	}
	.category-hero .category{ font-size: 2.0em; margin-top: -130px; }
	.category-hero .contenu .inner{ position: relative; padding: 0 15px 30px; }
	.category-hero .textes{ padding: 15px 0 30px; }
	.category-hero .textes .firsttitle{
		font-size: 2.4em;
	}
	.category-hero .textes .firsttitle + h3{
		font-size: 2.0em;
	}
	.category-hero .textes p{ font-size: 1.3em; max-height: 290px; }
	.category-hero .textes p.date{ font-size: 1.0em; bottom: 15px; }
</style>
<div class="category-hero">
	<img src="{{ asset('/resources/img/image-7@3x.jpg?v=2') }}" alt="">
	<div class="contenu">
		<div class="inner">
			<h3 class="category">éducation</h3>
			<div class="textes">
				<h3 class="firsttitle">Voici pourquoi</h3>
				<h3>Nos collégiens ont progressé</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste impedit necessitatibus nemo, libero expedita possimus fuga dolorum molestiae soluta ducimus nisi laborum quidem corporis aspernatur commodi, ut earum sequi. Soluta. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, perferendis delectus magnam suscipit exercitationem cupiditate! Deleniti et nisi eum soluta obcaecati, velit delectus praesentium explicabo dolor, deserunt, impedit voluptate doloribus.</p>

				<p class="date">10-11-2019</p>
			</div>
		</div>
	</div>
</div>

<div class="home tickets">
	<div class="container">
		<div class="row">
			<div class="col s6 ticket dual">
				<a href="#">
					<h3 class="category">Collège</h3>
					<div class="inner">
						<img src="{{ asset('/resources/img/image-12.jpg') }}" alt="" class="ticket-image">

						<div class="textes">
							<div class="relative">
								<h3 class="firsttitle">5 conseils pour </h3>
								<h3>réussir son entrée en sixième</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</p>

								<p class="date">10-11-2019</p>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col s6 ticket dual">
				<h3 class="category">Lycée</h3>
				<div class="inner">
					<img src="{{ asset('/resources/img/image-13.jpg') }}" alt="" class="ticket-image">

					<div class="textes">
						<div class="relative">
							<h3 class="firsttitle">Jeunes filles scolarisées :</h3>
							<h3>une chance pour le pays</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</p>

							<p class="date">10-11-2019</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="hero-discover">
	<div class="container">
		<div class="zone videothumb">
			<img src="{{ asset('/resources/img/image-8.jpg') }}" alt="">
			
			<div class="playeric">
				<div class="icon-noun-play-939124"></div>
			</div>
			
			<div class="text">
				<h3>Parité dans l’éducation</h3>
			</div>

			<h3 class="hashtag">#mat</h3>
		</div>

		<div class="flexzone">
			<a href="#">éducation</a>
			<a href="#">Sécurité</a>
			<a href="#">Emploi</a>
			<a href="#">Paix et unité</a>
			<a href="#">Pouvoir d'achat</a>
			<a href="#">Santé</a>
		</div>
	</div>
</div>

@include('web.include.bandes')
@endsection