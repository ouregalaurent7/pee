<!-- Header -->
<header class="header {{!Route::is('web') ? 'header-page' : ''}}">
    <div class="header-fixed">
        <nav class="navbar navbar-expand-lg header-nav scroll-sticky">
            <div class="container">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{route('web')}}" class="navbar-brand logo">
                        <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('web')}}" class="menu-logo">
                            <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">
                        <li class="has-submenu active">
                            <a class="" href="{{route('web')}}">Accueil </a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{route('w.formation')}}">Formations </a>
                        </li>
                        <li class="has-submenu">
                            <a href="javascript:void();">Offres <i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu first-submenu">
                                <li><a href="{{route('w.offreItem')}}">Emplois</a></li>
                                <li><a href="{{route('w.offreItem')}}">Stages</a></li>
                                <li><a href="{{route('w.offreItem')}}">Financement</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="{{route('w.promo')}}">Promotion </a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{route('w.news')}}">Actualités </a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{route('w.contact')}}">Contact </a>
                        </li>
                        @if (Auth::check())
                            <li class="login-link">
                                <a href="javascript:void();" class="dropdown-toggle">
                                    <span class="user-img">
                                        @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                        <span class="status online">Bienvenue {{substr(ucfirst(Auth::user()->name),0,7)}}...</span>
                                        <h6></h6>
                                    </span>
                                </a>
                                <ul class="submenu first-submenu">
                                    <a class="p-1 mt-2" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                    <a class="p-1 m-1" href="{{route('ens.profiles')}}"><i class="fa fa-user-cog me-1"></i> Profil </a>
                                    <a class="p-1 mb-2" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                                </ul>
                            </li>
                        @else
                            <li class="login-link">
                                <a href="{{route('login')}}">Login / Signup</a>
                            </li>
                        @endif
                    </ul>
                </div>
                <ul class="nav header-navbar-rht">
                    @if (Auth::check())
                        <li class="nav-item">
                            <a href="instructor-chat.html"><img src="{{asset('assets/fronts/img/icon/messages.svg')}}" alt="img"></a>
                        </li>
                        <li class="nav-item noti-nav">
                            <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                                <img src="{{asset('assets/fronts/img/icon/notification.svg')}}" alt="notif">
                            </a>
                            <div class="notifications dropdown-menu dropdown-menu-right">
                                <div class="topnav-dropdown-header">
                                    <span class="notification-title">Notifications
                                    <select>
                                        <option>Tout</option>
                                        <option>Non lus</option>
                                    </select>
                                    </span>
                                    <a href="javascript:void(0)" class="clear-noti">Marquer tout comme lu <i class="fa-solid fa-circle-check"></i></a>
                                </div>
                                <div class="noti-content">
                                    <ul class="notification-list">
                                        <li class="notification-message">
                                            <div class="media d-flex">
                                                <div>
                                                    <a href="notifications.html" class="avatar">
                                                        {{-- <span class="fa fa-user avatar-img fa-2x"></span> --}}
                                                        <img class="avatar-img" alt="" src="{{('assets/fronts/img/user/user7.jpg')}}">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h6><a href="notifications.html">Lex Murphy requested <span>access to</span> UNIX directory tree hierarchy </a></h6>
                                                    <button class="btn btn-accept">Accept</button>
                                                    <button class="btn btn-reject">Reject</button>
                                                    <p>Today at 9:42 AM</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="notification-message">
                                            <div class="media d-flex">
                                                <div>
                                                    <a href="notifications.html" class="avatar">
                                                        <img class="avatar-img" alt="" src="{{('assets/fronts/img/user/user3.jpg')}}">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h6><a href="notifications.html">Dennis Nedry <span>commented on</span> Isla Nublar SOC2 compliance report</a></h6>
                                                    <p class="noti-details">“Oh, I finished de-bugging the phones, but the system's compiling for eighteen minutes, or twenty.  So, some minor systems may go on and off for a while.”</p>
                                                    <p>Yesterday at 5:42 PM</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item user-nav">
                            <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                                <span class="user-img">
                                    @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                    <span class="status online"></span>
                                </span>
                            </a>
                            <div class="users dropdown-menu dropdown-menu-right" data-popper-placement="bottom-end" >
                                <div class="user-header">
                                    <div class="avatar avatar-sm">
                                        @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                    </div>
                                    <div class="user-text">
                                        <h6>{{ucwords(Auth::user()->name)}}</h6>
                                        <p class="text-muted mb-0">Instructeur</p>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                <a class="dropdown-item" href="{{route('ens.profiles')}}"><i class="feather-star me-1"></i> Profil </a>
                                <a class="dropdown-item" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                            </div>
                        </li>

                    @else
                        <li class="nav-item">
                            <a class="nav-link header-sign" href="{{route('login')}}">Connexion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link header-login" href="{{route('register')}}">Inscription</a>
                        </li>
                    @endif


                </ul>

            </div>
        </nav>
    </div>
</header>
<!-- /Header -->
