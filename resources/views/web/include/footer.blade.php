<!-- Footer -->
<footer class="foote">

    <!-- Footer Top -->
    <div class="footer-top aos" data-aos="">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-about">
                        <div class="footer-logo">
                            <img src="{{('assets/fronts/img/logo.png')}}" alt="logo">
                        </div>
                        <div class="footer-about-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat mauris Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat mauris</p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-2 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">Menu</h2>
                        <ul>
                            <li><a href="{{route('web')}}">Accueil</a></li>
                            {{-- <li><a href="{{route('w.formation')}}">Formations</a></li> --}}
                            <li><a href="{{route('w.offreItem')}}">Offres d'emploi</a></li>
                            <li><a href="{{route('w.offreItem')}}">Offres de stage</a></li>
                            <li><a href="{{route('w.offreItem')}}">Offres de financement</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-2 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">&nbsp;</h2>
                        <ul>
                            <li><a href="{{route('w.promo')}}">Promotion</a></li>
                            <li><a href="{{route('w.news')}}">Actualité</a></li>
                            <li><a href="{{route('w.contact')}}">Contact</a></li>
                            <li><a href="{{route('w.about')}}">A propos de PEE</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-4 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">Adresse</h2>
                        <div class="footer-contact-info">
                            <div class="footer-address">
                                <img src="{{asset('assets/fronts/img/icon/icon-20.svg')}}" alt="" class="img-fluid">
                                <p> Université Félix Houphouët-Boigny, Côcôdy<br> Abidjan, Côte d'Ivoire</p>
                            </div>
                            <p>
                                <img src="{{asset('assets/fronts/img/icon/cloud.svg')}}" alt="" class="img-fluid">
                                <a href="www.ufhb.pee.ci" class="">www.ufhb.pee.ci</a>
                            </p>
                            <p>
                                <img src="{{asset('assets/fronts/img/icon/icon-19.svg')}}" alt="" class="img-fluid">
                                <a href="mailto:infos@ufhb.pee.ci">infos@ufhb.pee.ci</a>
                            </p>
                            <p class="mb-0">
                                <img src="{{asset('assets/fronts/img/icon/icon-21.svg')}}" alt="" class="img-fluid">
                                <a href="tel:+225 01 73 50 57 57">+225 01 73 50 57 57</a>

                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6">
                        <div class="privacy-policy">
                            <ul>
                                <li><a href="{{route('w.terme')}}">Termes d'utilisation</a></li>
                                <li><a href="{{route('w.politique')}}">Politique de confidentialité</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="copyright-text">
                            <p class="mb-0">&copy; 2023-{{date('Y')}} PEE. Tout droit réservé.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
<!-- /Footer -->
