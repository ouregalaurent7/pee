@if(Session::has('message'))
    <div style="font-size: 16px;" class="alert alert-warning alert-dismissible fade show" role="alert">
        <i class="uil uil-check mr-2"></i>
        {!! Session::get('message') !!}
    </div>
@endif
@if(Session::has('success'))
    <div style="font-size: 16px;" class="alert alert-success alert-dismissible fade show label-light-success" role="alert">
        <i class="uil uil-check mr-2"></i>
        {!! Session::get('success') !!}
    </div>
@endif
@if(Session::has('error'))
    <div style="font-size: 16px;" class="alert alert-danger alert-dismissible fade show label-light-danger" role="alert">
        <i class="uil uil-exclamation-octagon mr-2"></i>
        {!! Session::get('error') !!}
    </div>
@endif
