<!-- Footer -->
<footer class="foote">


    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6">
                        <div class="privacy-policy">
                            <ul>
                                <li><a target="_blank" href="{{route('w.terme')}}">Termes d'utilisation</a></li>
                                <li><a target="_blank" href="{{route('w.politique')}}">Politique de confidentialité</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="copyright-text">
                            <p class="mb-0">&copy; 2023-{{date('Y')}} PEE. Tout droit réservé.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
<!-- /Footer -->
