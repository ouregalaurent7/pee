<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>@yield('title')</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/fronts/img/favicon.png') }}">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/bootstrap.min.css') }}">
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/all.min.css') }}">
		{{-- <!-- Owl Carousel CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.carousel.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.theme.default.min.css') }}"> --}}
		<!-- Slick CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/slick/slick.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/slick/slick-theme.css') }}">
		<!-- Aos CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/aos/aos.css') }}">
        <!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{ asset('assets/fronts/plugins/feather/feather.css') }}">
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/select2/css/select2.min.css') }}">
		<!-- Main CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/style.css') }}">

        @yield('css')

	</head>
	<body>

        <div class="main-wrapper">

			@include('ens.include.header')


            <!-- Page Wrapper -->
			<div class="page-content instructor-page-content">
				<div class="container">
					<div class="row">

						<!-- Sidebar -->
						<div class="col-xl-3 col-lg-4 col-md-12 theiaStickySidebar">
							@include('ens.include.navbar')
						</div>
						<!-- /Sidebar -->

						<!-- Instructor Dashboard -->
						<div class="col-xl-9 col-lg-8 col-md-12">
                            @yield('content')
						</div>
						<!-- /Instructor Dashboard -->

					</div>
				</div>
			</div>
			<!-- /Page Wrapper -->


            @include('ens.include.footer')

		</div>

        <!-- jQuery -->
		<script src="{{ asset('assets/fronts/js/jquery-3.6.0.min.js') }}"></script>
		<!-- Bootstrap Core JS -->
		<script src="{{ asset('assets/fronts/js/bootstrap.bundle.min.js') }}"></script>

		<!-- Select2 JS -->
		<script src="{{ asset('assets/fronts/plugins/select2/js/select2.min.js') }}"></script>
        <!-- Feature JS -->
		<script src="{{asset('assets/fronts/plugins/feather/feather.min.js')}}"></script>
        <!-- Sticky Sidebar JS -->
        <script src="{{asset('assets/fronts/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
        <script src="{{asset('assets/fronts/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

        <!-- counterup JS -->
		<script src="{{ asset('assets/fronts/js/jquery.waypoints.js') }}"></script>
		<script src="{{ asset('assets/fronts/js/jquery.counterup.min.js') }}"></script>
		{{-- <!-- Owl Carousel -->
		<script src="{{ asset('assets/fronts/js/owl.carousel.min.js') }}"></script> --}}
		<!-- Slick Slider -->
		<script src="{{ asset('assets/fronts/plugins/slick/slick.js') }}"></script>
		<!-- Aos -->
		<script src="{{ asset('assets/fronts/plugins/aos/aos.js') }}"></script>

		<!-- Custom JS -->
		<script src="{{ asset('assets/fronts/js/script.js') }}"></script>








        @yield('js')

	</body>
</html>
