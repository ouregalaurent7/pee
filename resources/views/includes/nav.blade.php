<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                {{--<li class="text-muted menu-title">Navigation</li>--}}

                <li> <a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span> Tableau de bord </span></a></li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"></i><span> Articles </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('news')}}">Tous les articles</a></li>
                        <li><a href="{{route('news.create')}}">Ajouter</a></li>
                        @if(Auth::user()->role_id==1)
                        <li><a href="{{route('categories')}}">Catégories</a></li>
                        @endif
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-graduate"></i><span> Étudiants </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('etudiants')}}">Tous les étudiants</a></li>
                        <li><a href="{{route('etudiants.create')}}">Ajouter</a></li>
                        <li><a href="{{route('etudiants.indicateurs')}}">Indicateurs</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-book"></i><span> Formations </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('formations')}}">Tous les formations</a></li>
                        <li><a href="{{route('formations.create')}}">Ajouter</a></li>
                    </ul>
                </li>
                @if(Auth::user()->role_id==1)

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Utilisateurs </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('users')}}">Tous les utilisateurs</a></li>
                        <li><a href="{{route('users.create')}}">Ajouter</a></li>
                        <li><a href="{{route('roles')}}">Rôles</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-image"></i><span> Médiathèques </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('albums')}}">Photos</a></li>
                        <li><a href="{{route('videos')}}">Vidéos</a></li>
                    </ul>
                </li>

                @endif
                {{--<li class="has_sub">--}}
                {{--<a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Icons </span> <span class="menu-arrow"></span> </a>--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                {{--<li><a href="icons-simple-line.html">Simple line Icons</a></li>--}}
                {{--<li><a href="icons-weather.html">Weather Icons</a></li>--}}
                {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
