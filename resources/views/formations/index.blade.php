@extends('layouts.app')

@section('title')
    Formations
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')

    <script type="text/javascript" src="{{ url('assets/plugins/isotope/js/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}
    <script type="text/javascript">
        $(window).load(function(){
            var $container = $('.portfolioContainer');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            $('.portfolioFilter a').click(function(){
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });
        });
        $(document).ready(function() {
            $('.image-popup').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                }
            });
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Formations
                            <a href="{{route('etudiants.create')}}" class="btn btn-default btn-md waves-effect waves-light" style="float: right;"><i class="md md-add"></i> Ajouter</a>
                        </h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">{{env('APP_NAME')}}</a>
                            </li>
                            <li class="active">
                                Formations
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-12">

                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <div class="portfolioFilter">
                            <a href="#" data-filter="*" class="current">All</a>
                            <a href="#" data-filter=".mobiles">Mobiles</a>
                            <a href="#" data-filter=".tablets">Tablets</a>
                            <a href="#" data-filter=".desktops">Desktops</a>
                            <a href="#" data-filter=".other">Other</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <form role="form">
                            <div class="form-group contact-search m-b-30">
                                <input type="text" id="search" class="form-control" placeholder="Search...">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>
                </div>

                <div class="row port">
                    <div class="portfolioContainer m-b-15">
                        <div class="col-sm-6 col-lg-4 mobiles">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-2.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->



                        <div class="col-sm-6 col-lg-4 mobiles">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-3.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->



                        <div class="col-sm-6 col-lg-4 other">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-4.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->


                        <div class="col-sm-6 col-lg-4 other">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-5.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->


                        <div class="col-sm-6 col-lg-4 tablets">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-6.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->



                        <div class="col-sm-6 col-lg-4 desktops">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-7.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->


                        <div class="col-sm-6 col-lg-4desktops">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-8.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->

                        <div class="col-sm-6 col-lg-4 tablets">
                            <div class="card-box">
                                <div class="contact-card">
                                    <a class="pull-left" href="#">
                                        <img class="img-circle" src="assets/images/users/avatar-9.jpg" alt="">
                                    </a>
                                    <div class="member-info">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                        <p class="text-muted">Branch manager</p>
                                        <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                        <div class="contact-action">
                                            <a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="md md-close"></i></a>
                                        </div>
                                    </div>

                                    <ul class="social-links list-inline m-0">
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                        </li>
                                        <li>
                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div> <!-- end col -->
                    </div>
                </div>



            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
