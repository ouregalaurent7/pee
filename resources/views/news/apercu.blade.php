@extends('layouts.app')

@section('title')
    {{$actu->title}}
    @parent
@stop

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}

@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-8">
                        @if($actu->type_article=='text')
                        <img src="{{asset('articles/'.$actu->img)}}" alt="{{$actu->title}}" class="img-responsive img-me">
                        @else
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{$actu->video}}"></iframe>
                                @php $video_id = explode("?v=", $actu->video); @endphp
                                <iframe width="100%" height="600" style="margin-bottom: 25px;" src="https://www.youtube.com/embed/{{$video_id[1]}}?autoplay=0&showinfo=0&controls=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        @endif
                        <div class="card-box m-t-20">
                            <h1>{{$actu->title}}</h1>
                            <div class="w-blogpost-meta">
                                <span class="w-blogpost"><i class="fa fa-clock-o"></i> {{$actu->created_at->format('D M Y')}}</span>
                                <span class="w-blogpost"><i class="fa fa-user"></i> {{$actu->author->name}}</span>
                                @php $cats = (new \App\Http\Controllers\NewsController())->getCatlibell($actu->cat_id) @endphp

                                <span class="w-blogpost"><i class="fa fa-folder"></i>
                                    @foreach($cats as $cat)
                                        {{$cat->libelle}} {{!$loop->last ? ',' : ''}}
                                    @endforeach
                                </span>
                            </div>

                            <div class="row m-t-40">
                                {!! $actu->des !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="card-box">
                                <h4 class="m-t-0 m-b-20 header-title"><b>Publier</b></h4>
                                <hr>
                                <div class="row">
                                    <form action="{{route('news.preview_store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{$actu->id}}" name="idactu">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="update"><i class="fa fa-edit"></i> Mise à jour</button>
                                        <br><br><button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="publier"><i class="fa fa-save"></i> Publier</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
