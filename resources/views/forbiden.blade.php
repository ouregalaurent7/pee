@extends('layouts.error')



@section('page_title')

    Erreur 500 | {{ env('APP_NAME') }}

@stop



@section('content')



    <div class="row">

        <div class="col-md-12">

            <div class="text-center">

                <div>

                    <div class="row justify-content-center">

                        <div class="col-sm-4">

                            <div class="error-img">

                                <img src="{{asset('assets/images/404-error.png')}}" alt="" class="img-fluid mx-auto d-block">

                            </div>

                        </div>

                    </div>

                </div>

                <h4 class="text-uppercase mt-4">Erreur 500</h4>

                <p class="text-muted text-danger">Vous ne disposez pas des droits d'accès à cette page !</p>

                <div class="mt-5">

                    <a class="btn btn-primary waves-effect waves-light" href="{{url('/')}}">Retour à l'accueil</a>
                    <a class="btn btn-danger waves-effect waves-light" href="{{url('/quicklogout')}}">Déconnexion</a>

                </div>

            </div>



        </div>

    </div>

    <!-- end row --> &nbsp;



@endsection



