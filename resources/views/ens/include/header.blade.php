<!-- Header -->
<header class="header header-page">
    <div class="header-fixed">
        <nav class="navbar navbar-expand-lg header-nav scroll-sticky">
            <div class="container header-border">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{route('web')}}" class="navbar-brand logo">
                        <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('web')}}" class="menu-logo">
                            <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">


                        <li class="login-link">
                            <a href="javascript:void();" class="dropdown-toggle">
                                <span class="user-img">
                                    <img src="{{('assets/fronts/img/profile-avatar.png')}}" alt="">
                                    <span class="status online">Bienvenue {{substr(ucfirst(Auth::user()->name),0,7)}}...</span>
                                    <h6></h6>
                                </span>
                            </a>
                            <ul class="submenu first-submenu">
                                <a class="p-1 mt-2" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                <a class="p-1 m-1" href="{{route('ens.profiles')}}"><i class="fa fa-user-cog me-1"></i> Profil </a>
                                <a class="p-1 mb-2" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                            </ul>
                        </li>
                    </ul>
                </div>
                <ul class="nav header-navbar-rht">
                    <li class="nav-item">
                        <a href="instructor-chat.html"><img src="{{asset('assets/fronts/img/icon/messages.svg')}}" alt="messages"></a>
                    </li>
                    <li class="nav-item noti-nav">
                        <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <img src="{{asset('assets/fronts/img/icon/notification.svg')}}" alt="notif">
                        </a>
                        <div class="notifications dropdown-menu dropdown-menu-right">
                            <div class="topnav-dropdown-header">
                                <span class="notification-title">Notifications
                                <select>
                                    <option>Tout</option>
                                    <option>Non lus</option>
                                </select>
                                </span>
                                <a href="javascript:void(0)" class="clear-noti">Marquer tout comme lu <i class="fa-solid fa-circle-check"></i></a>
                            </div>
                            <div class="noti-content">
                                <ul class="notification-list">
                                    <li class="notification-message">
                                        <div class="media d-flex">
                                            <div>
                                                <a href="notifications.html" class="avatar">
                                                    {{-- <span class="fa fa-user avatar-img fa-2x"></span> --}}
                                                    <img class="avatar-img" alt="" src="{{('assets/fronts/img/user/user7.jpg')}}">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h6><a href="notifications.html">Lex Murphy requested <span>access to</span> UNIX directory tree hierarchy </a></h6>
                                                <button class="btn btn-accept">Accept</button>
                                                <button class="btn btn-reject">Reject</button>
                                                <p>Today at 9:42 AM</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="notification-message">
                                        <div class="media d-flex">
                                            <div>
                                                <a href="notifications.html" class="avatar">
                                                    <img class="avatar-img" alt="" src="{{('assets/fronts/img/user/user3.jpg')}}">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h6><a href="notifications.html">Dennis Nedry <span>commented on</span> Isla Nublar SOC2 compliance report</a></h6>
                                                <p class="noti-details">“Oh, I finished de-bugging the phones, but the system's compiling for eighteen minutes, or twenty.  So, some minor systems may go on and off for a while.”</p>
                                                <p>Yesterday at 5:42 PM</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item user-nav">
                        <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <span class="user-img">
                                @if (Auth::user()->img != '')
                                    <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur">
                                @else
                                    <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur">
                                @endif
                                <span class="status online"></span>
                            </span>
                        </a>
                        <div class="users dropdown-menu dropdown-menu-right" data-popper-placement="bottom-end" >
                            <div class="user-header">
                                <div class="avatar avatar-sm">
                                    @if (Auth::user()->img != '')
                                        <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                    @else
                                        <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                    @endif
                                </div>
                                <div class="user-text">
                                    <h6>Jenny Wilson</h6>
                                    <p class="text-muted mb-0">Instructeur</p>
                                </div>
                            </div>
                            <a class="dropdown-item" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                            <a class="dropdown-item" href="{{route('ens.profiles')}}"><i class="feather-star me-1"></i> Profil </a>
                            <a class="dropdown-item" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                        </div>
                    </li>

                </ul>

            </div>
        </nav>
    </div>
</header>
<!-- /Header -->
