<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'throttled' => 'Veuillez réessayer plutard.',
    'password' => 'Les mots de passe doivent contenir au moins six caractères et être identiques.',
    'reset'    => 'Votre mot de passe a été réinitialisé !',
    'sent'     => 'Un mail de réinitialisation du mot de passe vous été envoyer ! Veuillez consulter votre boîte email ou vos spam pour continuer !',
    'token'    => "Ce jeton de réinitialisation de mot de passe est invalide.",
    'user'     => "Aucun utilisateur ne correspond à cette adresse email.",
    'Reset Password Notification' => 'Notification de réinitialisation de mot de passe',
    'You are receiving this email because we received a password reset request for your account.' => 'Vous avez reçu ce message car nous avons reçu une demande de réinitialisation de mot de passe pour votre compte.',
    'Reset Password' => 'Réinitialiser le mot de passe',
    'This password reset link will expire in :count minutes.' => 'Ce lien de réinitialisation expirera dans :count minutes.',
    'If you did not request a password reset, no further action is required.' => 'Si vous n\'avez pas demandé de réinitialisation de mot de passe, Veuillez aucune action n\'est requise.',


];
