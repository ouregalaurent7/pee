<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('web/index');
//});

use App\Login_activity;
use App\User;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'WebController@index')->name('web');
Route::get('apropos', 'WebController@about')->name('w.about');
Route::get('formations', 'WebController@formation')->name('w.formation');
Route::get('offre', 'WebController@offre')->name('w.offre');
Route::get('offre_item', 'WebController@offreItem')->name('w.offreItem');
Route::get('promo', 'WebController@promo')->name('w.promo');
Route::get('news', 'WebController@news')->name('w.news');
Route::get('news/detail', 'WebController@newsItem')->name('w.newsItem');
Route::get('contact', 'WebController@contact')->name('w.contact');
Route::get('terme-et-condition', 'WebController@terme')->name('w.terme');
Route::get('politique-de-confidentialite', 'WebController@politique')->name('w.politique');
Route::post('creer-un-compte-eudiant', 'WebController@store_user_etud')->name('w.store_user_etud');

Route::get('/category', function () {
    return view('web.category');
});


Route::get('/mobile', function () {
    return view('mobile.index');
});
Route::get('/mobile/category', function () {
    return view('mobile.category');
});




Auth::routes();
//forbiden route
Route::get('/forbiden', function () {
    return view('forbiden');
})->name('forbiden');
//quicklogout route
Route::get('/quicklogout', function () {
    if(Auth::user() == null) return redirect(route('login'));
    $user = User::findOrFail(Auth::user()->id);
    $user->update([
        'last_logout_at' => Carbon::now()->toDateTimeString(),
    ]);
    // update user login activity
    $activity_id = Login_activity::where('user_id', '=', Auth::user()->id)->max('id');
    $activity = Login_activity::find($activity_id);
    if($activity != null) {
        $activity->update([
            'logout_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
    Auth::logout();
    return redirect(route('web'));
})->name('quicklogout');

Route::prefix('guest')->middleware(['auth', 'isAdmin'])->group(function(){ //->namespace('Guest')
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    //NEWS
    Route::get('actualites', 'NewsController@index')->name('news');
    Route::get('actualites/create', 'NewsController@create')->name('news.create');
    Route::get('actualites/{id}/edit', 'NewsController@edit')->name('news.edit')->where('id', '[0-9]+');
    Route::get('actualites/{id}/retablir', 'NewsController@retablir')->name('news.retablir')->where('id', '[0-9]+');
    Route::get('actualites/type/{slug}', 'NewsController@showtype')->name('news.show_type')->where('slug', '[A-Za-z]+');
    Route::get('actualites/delete/{id}', 'NewsController@delet')->name('news.delete')->where('id', '[0-9]+');
    Route::get('actualites/deletes', 'NewsController@allDelet')->name('news.deletes');
    Route::get('actualites/brouillons', 'NewsController@allBrouillons')->name('news.brouillons');
    Route::get('actualites/publies', 'NewsController@allPublies')->name('news.publies');
    Route::get('actualites/retablirs', 'NewsController@allRetablirs')->name('news.retablirs');
    Route::get('actualites/{id}/apercu', 'NewsController@apercu')->name('news.apercu')->where('id', '[0-9]+');
    Route::get('actualites/{slug}', 'NewsController@show')->name('news.show');

    Route::post('actualites/store', 'NewsController@store')->name('news.store');
    Route::post('actualites/update', 'NewsController@update')->name('news.update');
    Route::post('actualites/preview/store', 'NewsController@previewStore')->name('news.preview_store');

    //CATEGORIE
    Route::get('categories', 'CategoriesController@index')->name('categories');
    Route::get('categories/{id}/edit', 'CategoriesController@edit')->name('categories.edit')->where('id', '[0-9]+');
    Route::get('categories/delete/{id}', 'CategoriesController@delet')->name('categories.delete')->where('id', '[0-9]+');
    Route::get('categories/deletes', 'CategoriesController@allDelet')->name('categories.deletes');

    Route::post('categories/store', 'CategoriesController@store')->name('categories.store');
    Route::post('categories/update', 'CategoriesController@update')->name('categories.update');

    //USERS
    Route::get('utilisateurs', 'UsersController@index')->name('users');
    Route::get('utilisateurs/create', 'UsersController@create')->name('users.create');
    Route::get('utilisateurs/{id}/edit', 'UsersController@edit')->name('users.edit')->where('id', '[0-9]+');
    Route::get('utilisateurs/delete/{id}', 'UsersController@delet')->name('users.delete')->where('id', '[0-9]+');
    Route::get('utilisateurs/deletes', 'UsersController@allDelet')->name('users.deletes');

    Route::post('utilisateurs/store', 'UsersController@store')->name('users.store');
    Route::post('utilisateurs/update', 'UsersController@update')->name('users.update');

    //ROLE
    Route::get('roles', 'RolesController@index')->name('roles');
    Route::get('roles/{id}/edit', 'RolesController@edit')->name('roles.edit')->where('id', '[0-9]+');
    Route::get('roles/delete/{id}', 'RolesController@delet')->name('roles.delete')->where('id', '[0-9]+');
    Route::get('roles/deletes', 'RolesController@allDelet')->name('roles.deletes');

    Route::post('roles/store', 'RolesController@store')->name('roles.store');
    Route::post('roles/update', 'RolesController@update')->name('roles.update');

    //PROFIL
    Route::get('profile', 'ProfilesController@index')->name('profiles');
    Route::post('profile/save', 'ProfilesController@profilupdate')->name('profiles.save');
    Route::post('profile/pass', 'ProfilesController@profileditpass')->name('profiles.savepass');
    Route::post('profile/avatar', 'ProfilesController@profileditavatar')->name('profiles.saveavatar');

    //MEDIATHEQUE

    //ALBUM PHOTO
    Route::get('albums', 'MediasController@album')->name('albums');
    Route::get('albums/delete/{id}', 'MediasController@deletAlbum')->name('albums.delete')->where('id', '[0-9]+');
    Route::get('albums/{id}/edit', 'MediasController@editAlbum')->name('albums.edit')->where('id', '[0-9]+');
    Route::get('albums/deletes', 'MediasController@allDeletAlbum')->name('albums.deletes');

    Route::post('albums/store', 'MediasController@storeAlbum')->name('albums.store');
    Route::post('albums/update', 'MediasController@updateAlbum')->name('albums.update');

    //PHOTO
    Route::get('photos/{id}', 'MediasController@photo')->name('photos')->where('id', '[0-9]+');
    Route::get('photos/{id}/{type}', 'MediasController@updatePhoto')->name('photos.update')->where(['id'=>'[0-9]+','type' => '[a-z]+']);
    Route::get('photos/delete/{id}', 'MediasController@deletPhoto')->name('photos.delete')->where('id', '[0-9]+');
    Route::get('photos/deletes', 'MediasController@allDeletPhoto')->name('photos.deletes');
    Route::get('photos/active', 'MediasController@allActPhoto')->name('photos.actives');
    Route::get('photos/inactive', 'MediasController@allInactPhoto')->name('photos.inactives');

    Route::post('photos/store', 'MediasController@storePhoto')->name('photos.store');

    //VIDEO
    Route::get('videos', 'MediasController@videos')->name('videos');
    Route::get('videos/delete/{id}', 'MediasController@deletVideo')->name('videos.delete')->where('id', '[0-9]+');
    Route::get('videos/deletes', 'MediasController@allDeletVideo')->name('videos.deletes');

    Route::post('videos/store', 'MediasController@storeVideo')->name('videos.store');

    // formations
    Route::get('formations', 'FormationsController@index')->name('formations');
    Route::get('formations/create', 'FormationsController@create')->name('formations.create');
    Route::post('formations/store', 'FormationsController@store')->name('formations.store');
    Route::get('formations/{id}/edit', 'FormationsController@edit')->name('formations.edit');
    Route::post('formations/update', 'FormationsController@update')->name('formations.update');
    Route::get('formations/delete/{id}', 'FormationsController@delet')->name('formations.delete');
    Route::get('formations/deletes', 'FormationsController@allDelet')->name('formations.deletes');
    //etudiants
    Route::get('etudiants', 'EtudiantsController@index')->name('etudiants');
    Route::get('etudiants/show/{id}', 'EtudiantsController@show')->name('etudiants.show');
    Route::get('etudiants/create', 'EtudiantsController@create')->name('etudiants.create');
    Route::post('etudiants/store', 'EtudiantsController@store')->name('etudiants.store');
    Route::get('etudiants/{id}/edit', 'EtudiantsController@edit')->name('etudiants.edit');
    Route::post('etudiants/update', 'EtudiantsController@update')->name('etudiants.update');
    Route::get('etudiants/delete/{id}', 'EtudiantsController@delete')->name('etudiants.delete');
    Route::get('etudiants/deletes', 'EtudiantsController@allDelete')->name('etudiants.deletes');
    Route::get('etudiants/disable', 'EtudiantsController@disable')->name('etudiants.disable');
    Route::get('etudiants/enable/{id}', 'EtudiantsController@enable')->name('etudiants.enable');
    // etudiant indicateurs de performance
    Route::get('etudiants/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.indicateurs');
    Route::get('etudiants/{id}/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.getindicateurs');
    Route::post('etudiants/{id}/indicateurs/store', 'EtudiantsController@storeIndicateurs')->name('etudiants.indicateurs.store');
    Route::get('etudiants/{id}/indicateurs/{idInd}/edit', 'EtudiantsController@editIndicateurs')->name('etudiants.indicateurs.edit')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
    Route::post('etudiants/{id}/indicateurs/update', 'EtudiantsController@updateIndicateurs')->name('etudiants.indicateurs.update');
    Route::get('etudiants/{id}/indicateurs/{idInd}/delete', 'EtudiantsController@deletIndicateurs')->name('etudiants.indicateurs.delete')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
    //enseignants
    Route::get('enseignants', 'EnseignantsController@index')->name('enseignants');
    Route::get('enseignants/create', 'EnseignantsController@create')->name('enseignants.create');
    Route::post('enseignants/store', 'EnseignantsController@store')->name('enseignants.store');
    Route::get('enseignants/{id}/edit', 'EnseignantsController@edit')->name('enseignants.edit');
    Route::post('enseignants/update', 'EnseignantsController@update')->name('enseignants.update');
    Route::get('enseignants/delete/{id}', 'EnseignantsController@delete')->name('enseignants.delete');
    //COURSES
});

Route::prefix('ens')->namespace('Ens')->middleware(['auth', 'isEnseignant'])->group(function(){
    Route::get('/', 'IndexController@index')->name('ens.home');
    //PROFIL
    Route::get('profile', 'ProfilController@index')->name('ens.profiles');
    Route::post('profile/save', 'ProfilController@profilupdate')->name('ens.profiles.save');
    Route::post('profile/pass', 'ProfilController@profileditpass')->name('ens.profiles.savepass');
    Route::post('profile/avatar', 'ProfilController@profileditavatar')->name('ens.profiles.saveavatar');
    //FORMATIONS
    Route::get('formations', 'FormationController@index')->name('ens.formations');
    Route::get('formations/create', 'FormationController@create')->name('ens.formations.create');
    Route::post('formations/store', 'FormationController@store')->name('ens.formations.store');
    Route::get('formations/{id}/edit', 'FormationController@edit')->name('ens.formations.edit')->where('id', '[0-9]+');
    Route::post('formations/{id}/update', 'FormationController@update')->name('ens.formations.update')->where('id', '[0-9]+');
    Route::get('formations/{id}/delete', 'FormationController@delete')->name('ens.formations.delete')->where('id', '[0-9]+');
    Route::get('formations/{id}/retablir', 'FormationController@retablir')->name('ens.formations.retablir')->where('id', '[0-9]+');
    //ETUDIANTS
    Route::get('etudiants', 'EtudiantController@index')->name('ens.etudiants');
    Route::get('etudiants/{id}/show', 'EtudiantController@show')->name('ens.etudiants.show')->where('id', '[0-9]+');
    Route::get('etudiants/{id}/delete', 'EtudiantController@delete')->name('ens.etudiants.delete')->where('id', '[0-9]+');
    Route::get('etudiants/{id}/retablir', 'EtudiantController@retablir')->name('ens.etudiants.retablir')->where('id', '[0-9]+');
    //COURSES
});


Route::prefix('etu')->namespace('Etu')->middleware(['auth', 'isEtudiant'])->group(function(){
    Route::get('/', 'IndexController@index')->name('ens.home');
});

/* Route::middleware(['auth', 'isAdmin'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    //NEWS
    Route::get('actualites', 'NewsController@index')->name('news');
    Route::get('actualites/create', 'NewsController@create')->name('news.create');
    Route::get('actualites/{id}/edit', 'NewsController@edit')->name('news.edit')->where('id', '[0-9]+');
    Route::get('actualites/{id}/retablir', 'NewsController@retablir')->name('news.retablir')->where('id', '[0-9]+');
    Route::get('actualites/type/{slug}', 'NewsController@showtype')->name('news.show_type')->where('slug', '[A-Za-z]+');
    Route::get('actualites/delete/{id}', 'NewsController@delet')->name('news.delete')->where('id', '[0-9]+');
    Route::get('actualites/deletes', 'NewsController@allDelet')->name('news.deletes');
    Route::get('actualites/brouillons', 'NewsController@allBrouillons')->name('news.brouillons');
    Route::get('actualites/publies', 'NewsController@allPublies')->name('news.publies');
    Route::get('actualites/retablirs', 'NewsController@allRetablirs')->name('news.retablirs');
    Route::get('actualites/{id}/apercu', 'NewsController@apercu')->name('news.apercu')->where('id', '[0-9]+');
    Route::get('actualites/{slug}', 'NewsController@show')->name('news.show');

    Route::post('actualites/store', 'NewsController@store')->name('news.store');
    Route::post('actualites/update', 'NewsController@update')->name('news.update');
    Route::post('actualites/preview/store', 'NewsController@previewStore')->name('news.preview_store');

    //CATEGORIE
    Route::get('categories', 'CategoriesController@index')->name('categories');
    Route::get('categories/{id}/edit', 'CategoriesController@edit')->name('categories.edit')->where('id', '[0-9]+');
    Route::get('categories/delete/{id}', 'CategoriesController@delet')->name('categories.delete')->where('id', '[0-9]+');
    Route::get('categories/deletes', 'CategoriesController@allDelet')->name('categories.deletes');

    Route::post('categories/store', 'CategoriesController@store')->name('categories.store');
    Route::post('categories/update', 'CategoriesController@update')->name('categories.update');

    //USERS
    Route::get('utilisateurs', 'UsersController@index')->name('users');
    Route::get('utilisateurs/create', 'UsersController@create')->name('users.create');
    Route::get('utilisateurs/{id}/edit', 'UsersController@edit')->name('users.edit')->where('id', '[0-9]+');
    Route::get('utilisateurs/delete/{id}', 'UsersController@delet')->name('users.delete')->where('id', '[0-9]+');
    Route::get('utilisateurs/deletes', 'UsersController@allDelet')->name('users.deletes');

    Route::post('utilisateurs/store', 'UsersController@store')->name('users.store');
    Route::post('utilisateurs/update', 'UsersController@update')->name('users.update');

    //ROLE
    Route::get('roles', 'RolesController@index')->name('roles');
    Route::get('roles/{id}/edit', 'RolesController@edit')->name('roles.edit')->where('id', '[0-9]+');
    Route::get('roles/delete/{id}', 'RolesController@delet')->name('roles.delete')->where('id', '[0-9]+');
    Route::get('roles/deletes', 'RolesController@allDelet')->name('roles.deletes');

    Route::post('roles/store', 'RolesController@store')->name('roles.store');
    Route::post('roles/update', 'RolesController@update')->name('roles.update');

    //PROFIL
    Route::get('profile', 'ProfilesController@index')->name('profiles');
    Route::post('profile/save', 'ProfilesController@profilupdate')->name('profiles.save');
    Route::post('profile/pass', 'ProfilesController@profileditpass')->name('profiles.savepass');
    Route::post('profile/avatar', 'ProfilesController@profileditavatar')->name('profiles.saveavatar');

    //MEDIATHEQUE

    //ALBUM PHOTO
    Route::get('albums', 'MediasController@album')->name('albums');
    Route::get('albums/delete/{id}', 'MediasController@deletAlbum')->name('albums.delete')->where('id', '[0-9]+');
    Route::get('albums/{id}/edit', 'MediasController@editAlbum')->name('albums.edit')->where('id', '[0-9]+');
    Route::get('albums/deletes', 'MediasController@allDeletAlbum')->name('albums.deletes');

    Route::post('albums/store', 'MediasController@storeAlbum')->name('albums.store');
    Route::post('albums/update', 'MediasController@updateAlbum')->name('albums.update');

    //PHOTO
    Route::get('photos/{id}', 'MediasController@photo')->name('photos')->where('id', '[0-9]+');
    Route::get('photos/{id}/{type}', 'MediasController@updatePhoto')->name('photos.update')->where(['id'=>'[0-9]+','type' => '[a-z]+']);
    Route::get('photos/delete/{id}', 'MediasController@deletPhoto')->name('photos.delete')->where('id', '[0-9]+');
    Route::get('photos/deletes', 'MediasController@allDeletPhoto')->name('photos.deletes');
    Route::get('photos/active', 'MediasController@allActPhoto')->name('photos.actives');
    Route::get('photos/inactive', 'MediasController@allInactPhoto')->name('photos.inactives');

    Route::post('photos/store', 'MediasController@storePhoto')->name('photos.store');

    //VIDEO
    Route::get('videos', 'MediasController@videos')->name('videos');
    Route::get('videos/delete/{id}', 'MediasController@deletVideo')->name('videos.delete')->where('id', '[0-9]+');
    Route::get('videos/deletes', 'MediasController@allDeletVideo')->name('videos.deletes');

    Route::post('videos/store', 'MediasController@storeVideo')->name('videos.store');

    // formations
    Route::get('formations', 'FormationsController@index')->name('formations');
    Route::get('formations/create', 'FormationsController@create')->name('formations.create');
    Route::post('formations/store', 'FormationsController@store')->name('formations.store');
    Route::get('formations/{id}/edit', 'FormationsController@edit')->name('formations.edit');
    Route::post('formations/update', 'FormationsController@update')->name('formations.update');
    Route::get('formations/delete/{id}', 'FormationsController@delet')->name('formations.delete');
    Route::get('formations/deletes', 'FormationsController@allDelet')->name('formations.deletes');
    //etudiants
    Route::get('etudiants', 'EtudiantsController@index')->name('etudiants');
    Route::get('etudiants/show/{id}', 'EtudiantsController@show')->name('etudiants.show');
    Route::get('etudiants/create', 'EtudiantsController@create')->name('etudiants.create');
    Route::post('etudiants/store', 'EtudiantsController@store')->name('etudiants.store');
    Route::get('etudiants/{id}/edit', 'EtudiantsController@edit')->name('etudiants.edit');
    Route::post('etudiants/update', 'EtudiantsController@update')->name('etudiants.update');
    Route::get('etudiants/delete/{id}', 'EtudiantsController@delete')->name('etudiants.delete');
    Route::get('etudiants/deletes', 'EtudiantsController@allDelete')->name('etudiants.deletes');
    Route::get('etudiants/disable', 'EtudiantsController@disable')->name('etudiants.disable');
    Route::get('etudiants/enable/{id}', 'EtudiantsController@enable')->name('etudiants.enable');
    // etudiant indicateurs de performance
    Route::get('etudiants/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.indicateurs');
    Route::get('etudiants/{id}/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.getindicateurs');
    Route::post('etudiants/{id}/indicateurs/store', 'EtudiantsController@storeIndicateurs')->name('etudiants.indicateurs.store');
    Route::get('etudiants/{id}/indicateurs/{idInd}/edit', 'EtudiantsController@editIndicateurs')->name('etudiants.indicateurs.edit')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
    Route::post('etudiants/{id}/indicateurs/update', 'EtudiantsController@updateIndicateurs')->name('etudiants.indicateurs.update');
    Route::get('etudiants/{id}/indicateurs/{idInd}/delete', 'EtudiantsController@deletIndicateurs')->name('etudiants.indicateurs.delete')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
    //enseignants
    Route::get('enseignants', 'EnseignantsController@index')->name('enseignants');
    Route::get('enseignants/create', 'EnseignantsController@create')->name('enseignants.create');
    Route::post('enseignants/store', 'EnseignantsController@store')->name('enseignants.store');
    Route::get('enseignants/{id}/edit', 'EnseignantsController@edit')->name('enseignants.edit');
    Route::post('enseignants/update', 'EnseignantsController@update')->name('enseignants.update');
    Route::get('enseignants/delete/{id}', 'EnseignantsController@delete')->name('enseignants.delete');

}); */

