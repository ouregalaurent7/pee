-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 12 déc. 2019 à 07:58
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

CREATE TABLE `actualites` (
  `id` int(6) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `title` varchar(225) NOT NULL,
  `des` longtext NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `type_article` varchar(100) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `video` varchar(225) DEFAULT NULL,
  `cat_id` varchar(225) NOT NULL,
  `auteur` int(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `actualites`
--

INSERT INTO `actualites` (`id`, `slug`, `title`, `des`, `status`, `type_article`, `img`, `video`, `cat_id`, `auteur`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'mon-titre', 'Mon titre', '<p>Mon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titre</p>', '0', 'text', 'QkPrtLbt.jpeg', NULL, '[\"2\",\"3\"]', 1, '2019-10-18 17:34:13', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(2, 'mon-tritre-deux', 'Mon Tritre deux edit', '<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>\r\n<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>\r\n<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>', '1', 'text', 'HKsPHT51.jpg', NULL, '[\"1\",\"4\"]', 1, '2019-10-18 18:27:46', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(3, 'live-43-nouvelles-version-de-grafikartfr-questions-diverses', 'Live #43 : Nouvelles version de Grafikart.fr & questions diverses', '<p>Je n\'ai pas encore le sujet ;)</p>', '1', 'video', NULL, 'https://www.youtube.com/watch?v=ZoFZl4R-ANI', '[\"3\"]', 1, '2019-10-18 19:36:50', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(4, 'the-fed-just-printed-more-money-than-bitcoins-entire-market-cap', 'The Fed Just Printed More Money Than Bitcoin’s Entire Market Cap', '<p dir=\"ltr\">Bitcoin (<a href=\"https://cointelegraph.com/bitcoin-price-index\">BTC</a>) proponents are voicing fresh alarm after the United States&nbsp;<a href=\"https://cointelegraph.com/tags/federal-reserve\">Federal Reserve</a>&nbsp;printed more than its entire market cap in new money this month.</p>\r\n<h2>Fed balance sheet approaches $4T</h2>\r\n<p>As noted by cryptocurrency social media pundit&nbsp;<a href=\"https://twitter.com/Xentagz/status/1186180739206991872\" target=\"_blank\" rel=\"noopener nofollow\">Dennis Parker</a>&nbsp;on Oct. 21, since mid-September, the Fed has injected $210 billion into the economy.&nbsp;</p>\r\n<p>Part of its newly-revitalized quantitative easing (QE) strategy, the move dwarfs the total market cap of Bitcoin, which stands at $148 billion.&nbsp;</p>\r\n<p>QE refers to the buying up of government bonds in order to provide economic stimulus. The Fed&rsquo;s balance sheet, Parker notes, jumped from $3.77 trillion last month to $3.97 trillion. It had previously been higher, while the Fed&rsquo;s own projections call for a balance sheet worth&nbsp;<a href=\"https://www.reuters.com/article/us-usa-fed-balancesheet/feds-balance-sheet-could-end-up-higher-than-4-trillion-projections-idUSKCN1VO21U\" target=\"_blank\" rel=\"noopener nofollow\" data-amp=\"https://mobile-reuters-com.cdn.ampproject.org/c/s/mobile.reuters.com/article/amp/idUSKCN1VO21U\">$4.7 trillion</a>&nbsp;by 2025.&nbsp;</p>\r\n<h2>World &ldquo;sleepwalking&rdquo; into the next financial crisis</h2>\r\n<p>For holders of assets that cannot have their supply inflated, such as&nbsp;<a href=\"https://cointelegraph.com/tags/gold\">gold</a>&nbsp;and Bitcoin, money printing has regularly sparked calls to decrease reliance on&nbsp;<a href=\"https://cointelegraph.com/tags/fiat-money\">fiat currency</a>.&nbsp;</p>\r\n<p>Parker&rsquo;s suggestion that investors should buy BTC now came amid warnings from even the fiat establishment itself about the ailing health of the banking system.&nbsp;</p>\r\n<p>In a&nbsp;<a href=\"https://www.theguardian.com/business/2019/oct/20/world-sleepwalking-to-another-financial-crisis-says-mervyn-king\" target=\"_blank\" rel=\"noopener nofollow\" data-amp=\"https://amp-theguardian-com.cdn.ampproject.org/c/s/amp.theguardian.com/business/2019/oct/20/world-sleepwalking-to-another-financial-crisis-says-mervyn-king\">speech</a>&nbsp;at the International Monetary Fund&rsquo;s general meeting last week, former Bank of England governor Mervyn King told attendees the world was &ldquo;sleepwalking&rdquo; into a financial crisis even worse than that of 2008.</p>\r\n<p>&ldquo;By sticking to the new orthodoxy of monetary policy and pretending that we have made the banking system safe, we are sleepwalking towards that crisis,&rdquo; he summarized.</p>\r\n<p>The concept that interventionist economic practices on the part of governments and central banks leads to financial destruction forms one of the&nbsp;<a href=\"https://cointelegraph.com/news/ex-cftc-chairman-us-must-create-an-independent-blockchain-dollar\">central tenets</a>&nbsp;of Saifedean Ammous&rsquo; &ldquo;The Bitcoin Standard.&rdquo;&nbsp;</p>\r\n<p>Released in March 2018, the book focuses on Bitcoin as it compares to fiat currency and commodities such as gold.&nbsp;</p>\r\n<p>As Cointelegraph&nbsp;<a href=\"https://cointelegraph.com/news/bitcoin-is-already-at-40-of-average-fiat-currency-lifespan-10-years\">noted</a>, at ten years old, Bitcoin has now lasted 40% of the average fiat currency&rsquo;s lifespan.</p>', '1', 'text', 'IsqLwI0D.jpg', NULL, '[\"1\",\"4\"]', 1, '2019-10-21 08:55:01', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(5, 'joel-robuchon-le-chef-le-plus-etoile-du-monde', 'JOËL ROBUCHON LE CHEF LE PLUS ÉTOILÉ DU MONDE !', '<p>Nomm&eacute; &laquo;&nbsp;cuisinier du si&egrave;cle&nbsp;&raquo; par le Gault et Millau en 1990, Jo&euml;l Robuchon est &agrave; ce jour le chef qui d&eacute;tient le plus d&rsquo;&eacute;toiles au monde au Guide Michelin. &Agrave; la t&ecirc;te de nombreux restaurants en France et &agrave; l&rsquo;&eacute;tranger,&nbsp; il est sans conteste(1),&nbsp; l&rsquo;un des meilleurs repr&eacute;sentants de la cuisine fran&ccedil;aise &agrave; travers le monde.</p>\r\n<p>&Agrave; ses d&eacute;buts, rien ne pr&eacute;destinait(2) le jeune homme &agrave; cette carri&egrave;re extraordinaire. Fils de ma&ccedil;on, ce Poitevin(3) n&eacute; &agrave; la fin de la Seconde guerre mondiale, pense d&rsquo;abord consacrer sa vie &agrave; Dieu. Il d&eacute;couvre la cuisine aupr&egrave;s des religieuses. Ayant besoin de trouver un travail pour vivre, il s&rsquo;oriente finalement vers cette discipline qui le d&eacute;tend et le passionne. Embauch&eacute; comme apprenti de cuisine au Relais de Poitiers &agrave; quinze ans, il devient en 1966 Compagnon du Tour de France. L&rsquo;occasion pour lui d&rsquo;am&eacute;liorer sa technique aupr&egrave;s des plus grands. Il parvient ainsi &agrave; devenir chef de cuisine &agrave; l&rsquo;H&ocirc;tel Concorde Lafayette de Paris, alors qu&rsquo;il n&rsquo;a que vingt-neuf ans. Deux ans plus tard, en 1976, il d&eacute;croche le titre tr&egrave;s convoit&eacute;(4) de &laquo;&nbsp;Meilleur ouvrier de France&nbsp;&raquo;. Sa carri&egrave;re est d&eacute;sormais lanc&eacute;e&nbsp;!<br />Apr&egrave;s avoir dirig&eacute; le restaurant de l&rsquo;H&ocirc;tel Nikko &agrave; Paris, il ouvre son premier restaurant en 1981, Le Jamin, qui lui donne, d&egrave;s sa premi&egrave;re ann&eacute;e d&rsquo;existence, une &eacute;toile au Michelin. Une &eacute;toile qui se transforme en deux puis en trois, les deux ann&eacute;es suivantes. Du jamais vu(5) dans l&rsquo;histoire de la Gastronomie&nbsp;! C&rsquo;est s&ucirc;r, l&rsquo;homme a un don(6). Sans m&ecirc;me avoir besoin d&rsquo;&eacute;crire une recette, il s&eacute;duit les gourmets les plus exigeants en travaillant avec des produits de qualit&eacute; qu&rsquo;il arrive &agrave; transcender(7), comme sa c&eacute;l&egrave;bre pur&eacute;e de pomme de terre truff&eacute;e, son bar (poisson) po&ecirc;l&eacute; &agrave; la citronnelle ou sa mythique gel&eacute;e de caviar &agrave; la cr&egrave;me de chou-fleur.</p>\r\n<p><strong>Une cuisine ouverte au grand public</strong></p>\r\n<p>Souhaitant faire profiter ses talents au plus grand nombre, il accepte, &agrave; partir de 1987, de devenir conseiller technique chez un grand groupe industriel&nbsp;: Fleury Michon. Un d&eacute;fi plus difficile &agrave; relever qu&rsquo;on ne le croit. &laquo;&nbsp;C&rsquo;est beaucoup plus dur pour moi de faire un plat pour Fleury Michon que dans un trois-&eacute;toiles&nbsp;&raquo; d&eacute;clarait-il r&eacute;cemment dans une interview, mais c&rsquo;est aussi une immense satisfaction car cela lui permet de toucher le grand public, en d&eacute;mocratisant(8) son art. Meilleur exemple de ce succ&egrave;s populaire&nbsp;: son fameux parmentier de canard qui&nbsp; atteint les 20 000 portions par semaine&nbsp;!</p>\r\n<p>Apr&egrave;s un passage &agrave; Tokyo o&ugrave; il cuisine au Ch&acirc;teau Restaurant Taillevent-Robuchon, il d&eacute;cide en 1994 d&rsquo;ouvrir un nouveau restaurant &agrave; Paris qui est imm&eacute;diatement d&eacute;sign&eacute; par l&rsquo;International Herald Tribune comme le &laquo;&nbsp;Meilleur restaurant du monde&nbsp;&raquo;. Rien que &ccedil;a&nbsp;!</p>\r\n<p><strong>Un infatigable cr&eacute;ateur de nouveaux concepts</strong></p>\r\n<p>Toujours en d&eacute;placement pour contr&ocirc;ler et assurer la r&eacute;putation de ses diff&eacute;rents restaurants &agrave; travers le monde, Jo&euml;l Robuchon cr&eacute;e au d&eacute;but des ann&eacute;es 2000 un nouveau concept de restaurants&nbsp;: L&rsquo;Atelier.<br />Sur une dominante de noir et de rouge, la cuisine y est ouverte sur la salle. Limit&eacute; &agrave; une trentaine de places, le lieu permet aux clients de voir les plats qui se pr&eacute;parent, ce qui donne un caract&egrave;re tr&egrave;s convivial(9) &agrave; l&rsquo;endroit. Devant le succ&egrave;s, Jo&euml;l Robuchon d&eacute;cline rapidement le concept sur tous les continents en s&rsquo;adaptant &agrave; chaque fois aux traditions culinaires de chaque pays o&ugrave; il s&rsquo;installe. En 2004, il propose une nouvelle id&eacute;e&nbsp;:&nbsp;la Table de Jo&euml;l Robuchon qui offre ses grands classiques en petites ou grandes portions. Jamais &agrave; court d&rsquo;id&eacute;es, il ouvre aussi de nouveaux restaurants gastronomiques trois-&eacute;toiles dans un ch&acirc;teau &agrave; la fran&ccedil;aise en plein c&oelig;ur de Tokyo, &agrave; Las Vegas, au MGM Grand, &agrave; Londres, Hong Kong, Nagoya,<br />Monaco, Taipei&hellip;</p>\r\n<p>Les prochaines &eacute;tapes&nbsp;: Bangkok, New York, Mumba&iuml; et Bordeaux&hellip; Assur&eacute;ment, la cuisine du chef fran&ccedil;ais n&rsquo;a pas fini de conqu&eacute;rir le monde&nbsp;!</p>', '1', 'text', '6p49DkWq.jpg', NULL, '[\"5\"]', 3, '2019-10-22 15:10:14', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(6, 'voici-pourquoi-nos-collegiens-ont-progresse', 'Voici pourquoi nos collégiens ont progressé', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'h1u3nI9h.jpg', NULL, '[\"1\"]', 1, '2019-12-12 06:30:59', '2019-12-12 06:30:59', NULL),
(7, 'toulepleu-un-gyneco-pas-comme-les-autres', 'Toulepleu Un gynéco pas comme les autres', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'h41aCIFQ.jpg', NULL, '[\"2\"]', 1, '2019-12-12 06:31:57', '2019-12-12 06:31:57', NULL),
(8, '4-pistes-pour-augmenter-le-pouvoir-dachat-des-ivoiriens', '4 pistes pour augmenter le pouvoir d’achat des Ivoiriens.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'UZADDrHR.jpg', NULL, '[\"3\"]', 1, '2019-12-12 06:32:45', '2019-12-12 06:32:45', NULL),
(9, 'abidjan-la-nuit-comment-la-police-veille-sur-nous', 'Abidjan la nuit : comment la police veille sur nous.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'Yi5x2ScF.jpg', NULL, '[\"4\"]', 1, '2019-12-12 06:33:32', '2019-12-12 06:33:32', NULL),
(10, 'unite-nationale-ce-que-les-chefs-coutumiers-ont-a-nous-apprendre', 'Unité nationale Ce que les chefs coutumiers ont à nous apprendre', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'y97BWTZj.jpg', NULL, '[\"5\"]', 1, '2019-12-12 06:34:17', '2019-12-12 06:34:17', NULL),
(11, 'il-ny-a-pas-de-petits-metiers', 'Il n’y a pas de « petits » métiers.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'hzYVR1Lf.jpg', NULL, '[\"6\"]', 1, '2019-12-12 06:35:01', '2019-12-12 06:35:01', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `actualites_categories`
--

CREATE TABLE `actualites_categories` (
  `id` int(6) NOT NULL,
  `cat_id` int(6) NOT NULL,
  `actu_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `albums`
--

CREATE TABLE `albums` (
  `id` int(6) NOT NULL,
  `title` varchar(225) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `cover` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `albums`
--

INSERT INTO `albums` (`id`, `title`, `slug`, `cover`, `created_at`, `updated_at`) VALUES
(1, 'Naruto&Boruto', 'narutoboruto', 'JdaijdaD.png', '2019-10-25 15:30:37', '2019-10-25 16:25:02');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(6) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `libelle`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'education', 'ÉDUCATION', 'EDUCATION', '2019-10-18 14:35:05', '2019-12-12 06:26:15', NULL),
(2, 'sante', 'SANTÉ', 'SANTE', '2019-10-18 14:35:05', '2019-12-12 06:25:50', NULL),
(3, 'pourvoir-dachat', 'POURVOIR D\'ACHAT', 'POURVOIR D\'ACHAT', '2019-10-18 14:36:04', '2019-12-12 06:24:36', NULL),
(4, 'securite', 'SÉCURITÉ', 'SÉCURITÉ', '2019-10-18 14:36:04', '2019-12-12 06:25:05', NULL),
(5, 'paix-et-unite-nationale', 'PAIX ET UNITÉ NATIONALE', 'PAIX ET UNITÉ NATIONALE', '2019-10-21 12:19:40', '2019-12-12 06:27:03', NULL),
(6, 'emploi', 'EMPLOI', 'EMPLOI', '2019-12-12 06:27:57', '2019-12-12 06:27:57', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE `photos` (
  `id` int(6) NOT NULL,
  `img` varchar(225) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `album_id` int(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `photos`
--

INSERT INTO `photos` (`id`, `img`, `status`, `album_id`, `created_at`, `updated_at`) VALUES
(1, 'lFEb3vIo.jpg', '1', 1, '2019-10-24 10:37:45', '2019-10-24 11:53:49'),
(2, 'TBwtf5Q0.jpg', '1', 1, '2019-10-24 10:40:09', '2019-10-24 11:53:49');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(6) NOT NULL,
  `libelle` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `libelle`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrateur', '2019-10-21 15:56:23', '2019-10-21 15:56:23', NULL),
(2, 'Editeur', '2019-10-21 15:56:23', '2019-10-21 15:56:23', NULL),
(3, 'Auteur', '2019-10-21 15:56:55', '2019-10-22 09:45:55', NULL),
(4, 'Superviseur', '2019-10-21 16:59:27', '2019-10-22 09:43:24', '2019-10-22 09:43:24');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(6) NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `sexe`, `email_verified_at`, `password`, `remember_token`, `role_id`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ourega Fabien', 'test@test.com', 'Homme', NULL, '$2y$10$UNLTWg.GQ1TPQhkHZBOVS.WFk6Mud3RPYtVtVxWlFzydgi4PzoPUy', NULL, 1, 'ba4qBAJ0.jpeg', '2019-10-17 17:52:35', '2019-10-22 14:27:01', NULL),
(2, 'Editeur', 'edit@edit.com', NULL, NULL, '$2y$10$PPkw7efBEQEU/vIdny2WO.BJAhNbD7uF.YYokfqhACBdRw6HQoIi6', NULL, 2, NULL, '2019-10-21 16:25:39', '2019-10-22 09:54:42', NULL),
(3, 'Auteur', 'use@use.com', NULL, NULL, '$2y$10$uMapB3uBpC5ft1XLj/GrouKHZ2IYc9daGwY0kwLjgLfWl4oNPmTqy', NULL, 3, NULL, '2019-10-21 16:50:17', '2019-10-22 09:54:42', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

CREATE TABLE `videos` (
  `id` int(6) NOT NULL,
  `title` varchar(225) NOT NULL,
  `img` varchar(225) NOT NULL,
  `lien` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `videos`
--

INSERT INTO `videos` (`id`, `title`, `img`, `lien`, `created_at`, `updated_at`) VALUES
(1, 'NARUTO | LA VRAIE PUISSANCE DE MINATO ET SES TECHNIQUES EXPLIQUÉES !', 'https://i.ytimg.com/vi/YW34jyl5Ufw/hqdefault.jpg', 'https://www.youtube.com/watch?v=YW34jyl5Ufw', '2019-10-24 15:04:27', '2019-10-24 15:04:27');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `actualites`
--
ALTER TABLE `actualites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `actualites_categories`
--
ALTER TABLE `actualites_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `actualites`
--
ALTER TABLE `actualites`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `actualites_categories`
--
ALTER TABLE `actualites_categories`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
