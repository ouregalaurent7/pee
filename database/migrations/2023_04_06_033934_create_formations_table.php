<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        _____________table attributes_________________
        titre
        catégorie
        niveau
        pre=éréquis
        description
        lien (salles virtuelles)
        salle (salles physiques)
        prix
        prix promotionnel
        coverimage
        mots clés
        statut : 0=non publié, 1=publié
        debut
        duréé
        professeur (nullable)
        etp_id=default(1)
        */
        Schema::create('formations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre',250);
            $table->text('description');
            $table->string('categorie',250);
            $table->string('niveau',250);
            $table->string('prerequis',250);
            $table->text('lien')->nullable();
            $table->string('salle',50);
            $table->double('prix')->nullable();
            $table->double('prixpromo')->nullable();
            $table->text('img')->nullable();
            $table->text('motscles')->nullable();
            $table->tinyInteger('statut')->default(0);
            $table->date('debut')->nullable();
            $table->string('duree')->nullable();
            $table->string('slug')->nullable();
            $table->bigInteger('professeur_id')->nullable()->unsigned()->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->bigInteger('user_id')->nullable()->unsigned()->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->bigInteger('etp_id')->unsigned()->default(1)->constrained('etps')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formations');
    }
}
